#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub mod gps {
    use std::time::Duration;

    #[repr(C)]
    #[derive(Copy, Clone)]
    pub struct Memory {
        pub x: f32,
        pub y: f32,
        pub seconds: u32,
        pub micros: u32,
    }

    #[link(wasm_import_module = "gps")]
    #[allow(dead_code)]
    extern "C" {
        pub fn available() -> bool;
        pub fn memmap(ptr: *mut Memory);
    }

    #[allow(dead_code)]
    pub fn duration(gps: &Memory) -> Duration {
        Duration::new(gps.seconds as u64, gps.micros * 1000)
    }
}

pub mod treads {
    #[link(wasm_import_module = "treads")]
    #[allow(dead_code)]
    extern "C" {
        pub fn available() -> bool;
        pub fn set_speed(left: f32, right: f32);
    }
}

pub mod legs {
    #[link(wasm_import_module = "legs")]
    #[allow(dead_code)]
    extern "C" {
        pub fn available() -> bool;
        pub fn step_towards(forward: f32, right: f32, turn: f32);
        pub fn progress() -> f32;
    }
}

pub mod status {
    #[link(wasm_import_module = "status")]
    #[allow(dead_code)]
    extern "C" {
        pub fn available() -> bool;
        pub fn set_utf8(ptr: *const u8, len: usize);
        pub fn append_utf8(ptr: *const u8, len: usize);
    }

    #[allow(dead_code)]
    pub fn set(text: &str) {
        unsafe { if available() { set_utf8(text.as_bytes().as_ptr(), text.as_bytes().len()) } }
    }

    #[allow(dead_code)]
    pub fn append(text: &str) {
        unsafe { if available() { set_utf8(text.as_bytes().as_ptr(), text.as_bytes().len()) } }
    }
}

pub mod sys {
    #[link(wasm_import_module = "status")]
    #[allow(dead_code)]
    extern "C" {
        pub fn shutdown() -> !;
    }
}

