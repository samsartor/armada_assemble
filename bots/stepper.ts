declare namespace api {
    namespace legs {
        @external("legs", "step_towards")
        export function step_towards(forward: f32, right: f32, turn: f32): void;
        @external("legs", "progress")
        export function progress(): f32;
    }
}

let index: u32 = 0;

export function step(): void {
    if (api.legs.progress() == 1.0) {
        index += 1;
        index &= 0b11111;
        switch (index >> 3) {
            case 0:
                api.legs.step_towards( 1.0,  0.0, 0.0);
                break;
            case 1:
                api.legs.step_towards( 0.0, -1.0, 0.0);
                break;
            case 2:
                api.legs.step_towards(-1.0,  0.0, 0.0);
                break;
            case 3:
                api.legs.step_towards( 0.0,  1.0, 0.0);
                break;
        }
    }
}
