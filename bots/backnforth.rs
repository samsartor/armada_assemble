use std::f32::NAN;

mod api;

const CIRCLE: f32 = 35. * 35.;
pub static mut DIR: f32 = 1.;
pub static mut CROSSED: bool = false;
pub static mut GPS: api::gps::Memory = api::gps::Memory { x: NAN, y: NAN, seconds: 0, micros: 0 };

#[no_mangle]
unsafe extern "C" fn init() {
    api::gps::memmap(&mut GPS);
    api::treads::set_speed(DIR, DIR);
}

#[no_mangle]
unsafe extern "C" fn step() {
    if GPS.x * GPS.x + GPS.y * GPS.y > CIRCLE {
        if !CROSSED {
            DIR *= -1.;
            api::treads::set_speed(DIR, DIR);
        }
        CROSSED = true;
    } else {
        CROSSED = false;
    }
}
