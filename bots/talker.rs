use std::f32::NAN;
use std::time::Duration;

mod api;

static mut INDEX: usize = 0;
static mut LAST_PRINT: Duration = Duration::from_secs(0);
static mut GPS: api::gps::Memory = api::gps::Memory { x: NAN, y: NAN, seconds: 0, micros: 0 };

static STATUSES: &[&str] = &[
    "Hello",
    "World",
];

#[no_mangle]
unsafe extern "C" fn init() {
    api::gps::memmap(&mut GPS);
}

#[no_mangle]
unsafe extern "C" fn step() {
    let now = api::gps::duration(&GPS);
    if now - LAST_PRINT > Duration::from_secs_f32(5.) {
        LAST_PRINT = now;
        api::status::set(STATUSES[INDEX % STATUSES.len()]);
        INDEX += 1;
    }
}
