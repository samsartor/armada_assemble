mod api;

static mut INDEX: u32 = 0;

#[no_mangle]
unsafe extern "C" fn step() {
    if api::legs::progress() == 1. {
        INDEX += 1;
        INDEX &= 0b11111;
        api::status::set(&INDEX.to_string());
        match INDEX >> 3 {
            0 => api::legs::step_towards( 1.,  0., 0.),
            1 => api::legs::step_towards( 0., -1., 0.),
            2 => api::legs::step_towards(-1.,  0., 0.),
            3 => api::legs::step_towards( 0.,  1., 0.),
            _ => (),
        }
    }
}
