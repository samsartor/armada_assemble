__attribute__((import_module("legs")))
__attribute__((import_name("step_towards"))) 
void api_legs_step_towards(float forward, float right, float turn);
__attribute__((import_module("legs")))
__attribute__((import_name("progress")))
float api_legs_progress();

static unsigned int INDEX = 0;

__attribute__((visibility("default")))
void step() {
    if (api_legs_progress() == 1.0) {
        INDEX += 1;
        INDEX &= 0b11111;
        switch (INDEX >> 3) {
            case 0:
                api_legs_step_towards( 1.0,  0.0, 0.0);
                break;
            case 1:
                api_legs_step_towards( 0.0, -1.0, 0.0);
                break;
            case 2:
                api_legs_step_towards(-1.0,  0.0, 0.0);
                break;
            case 3:
                api_legs_step_towards( 0.0,  1.0, 0.0);
                break;
        }
    }
}
