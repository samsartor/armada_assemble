use std::f32::NAN;
use std::time::Duration;

mod api;

const CIRCLE: f32 = 35. * 35.;
pub static mut CROSSED: bool = false;
pub static mut LAST_HIT: Duration = Duration::from_secs(0);
pub static mut GPS: api::gps::Memory = api::gps::Memory { x: NAN, y: NAN, seconds: 0, micros: 0 };

#[no_mangle]
unsafe extern "C" fn init() {
    api::gps::memmap(&mut GPS);
    api::treads::set_speed(5., 5.);
}

#[no_mangle]
unsafe extern "C" fn step() {
    let now = api::gps::duration(&GPS);
    if GPS.x * GPS.x + GPS.y * GPS.y > CIRCLE {
        if !CROSSED {
            CROSSED = true;
            api::treads::set_speed(-2., -2.);
            api::status::set("REVERSING");
        }
    } else {
        if CROSSED {
            CROSSED = false;
            LAST_HIT = now;
        }
        let elapsed = (now - LAST_HIT).as_secs_f32();
        if elapsed > 2.5 {
            api::treads::set_speed(5., 5.);
            api::status::set("");
        } else if elapsed > 2. {
            api::treads::set_speed(0.5, -0.5);
            api::status::set("TURNING");
        }
    }
}
