use failure::{err_msg, Error};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use std::fs::File;
use std::io::Read as _;
use std::time;
use std::path::{PathBuf, Path};
use std::collections::HashMap;
use specs::{World, WorldExt, Builder, ReadStorage, DispatcherBuilder, Join};
use wasmer_runtime::{compile, Module};
use imgui::{im_str, ImString, StyleColor};
use glob::glob;

mod entity;
mod wutils;

const WINDOW_SIZE: u32 = 1024;

fn axis2world(v: f32) -> f32 {
    (WINDOW_SIZE as f32) * (v / 100. + 0.5)
}

fn point2world(loc: &entity::PlaneLocation, x: f32, y: f32) -> [f32; 2] {
    let [x, y] = loc.transform(x, y);
    [
        axis2world(x),
        axis2world(y),
    ]
}

fn create_rover(world: &mut World, driver: entity::EntityDriver, color: [f32; 4], rover_type: usize) {
    let mut builder = world.create_entity()
        .with(driver)
        .with(entity::PlaneLocation::default())
        .with(entity::Gps::default())
        .with(entity::Preview { color, radius: 1. })
        .with(entity::Status::default());
    match rover_type {
        1 => builder = builder.with(entity::Treads {
            radius: 1.,
            max_acc: 7.5,
            max_vel: 5.,
            .. Default::default()
        }),
        2 => builder = builder.with(entity::Legs {
            max_reach: 1.,
            max_turn: std::f32::consts::PI,
            period: 0.5,
            .. Default::default()
        }),
        _ => (),
    }
    builder.build();
}

pub fn main() -> Result<(), Error> {
    let sdl_context = sdl2::init().map_err(err_msg)?;
    let video_subsystem = sdl_context.video().map_err(err_msg)?;

    {
        let gl_attr = video_subsystem.gl_attr();
        gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
        gl_attr.set_context_version(3, 0);
    }

    let window = video_subsystem
        .window("Armada Assemble", WINDOW_SIZE, WINDOW_SIZE)
        .position_centered()
        .opengl()
        .allow_highdpi()
        .build()?;

    let mut imgui = imgui::Context::create();
    imgui.set_ini_filename(None);
    let mut imgui_sdl2 = imgui_sdl2::ImguiSdl2::new(&mut imgui, &window);
    let _gl_context = window.gl_create_context().map_err(err_msg)?;
    let imgui_renderer = imgui_opengl_renderer::Renderer::new(&mut imgui, |s| {
        video_subsystem.gl_get_proc_address(s) as _
    });

    let mut bot_error = None;
    let mut bot_modules: HashMap<PathBuf, Option<Module>> = HashMap::new();
    let bots_root = Path::new("bots/").canonicalize()?;
    let mut load_bots = true;
    let mut bot_color: [f32; 4] = imgui.style()[StyleColor::Text];
    let mut bot_type = 0;

    let mut dispatcher = DispatcherBuilder::new()
        .with(
            entity::PreStepSystem { max_step: time::Duration::from_secs_f32(15f32.recip()) },
            "prestep",
            &[])
        .with(entity::TreadSimulationSystem, "treadsim", &["prestep"])
        .with(entity::LegsSimulationSystem, "legsim", &["prestep"])
        .with(entity::EntityDriverSystem, "driver", &["prestep"])
        .build();
    let mut world = World::new();
    world.insert(entity::GlobalInfo {
        wall_time: time::Instant::now(),
        gps_time: time::Duration::from_secs(0),
        delta_time: 0.,
    });
    dispatcher.setup(&mut world);

    let mut event_pump = sdl_context.event_pump().map_err(err_msg)?;
    let mut last_frame = time::Instant::now();
    'running: loop {
        for event in event_pump.poll_iter() {
            imgui_sdl2.handle_event(&mut imgui, &event);
            if imgui_sdl2.ignore_event(&event) {
                continue;
            }
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                _ => {}
            }
        }

        dispatcher.dispatch(&world);

        imgui_sdl2.prepare_frame(imgui.io_mut(), &window, &event_pump.mouse_state());

        let now = time::Instant::now();
        let dt = (now - last_frame).as_secs_f32();
        imgui.io_mut().delta_time = dt;
        last_frame = now;

        let ui = imgui.frame();

        {
            let size = WINDOW_SIZE as f32;
            let world_window = imgui::Window::new(im_str!("World"))
                .movable(false)
                .position([0., 0.], imgui::Condition::Always)
                .size([size, size], imgui::Condition::Always)
                .bg_alpha(1.)
                .bring_to_front_on_focus(false)
                .no_decoration()
                .begin(&ui).unwrap();
            let draw_list = ui.get_window_draw_list();
            draw_list.add_circle([size / 2., size / 2.], axis2world(35.) - axis2world(0.), ui.style_color(StyleColor::TextDisabled))
                .thickness(5.)
                .build();
            world.exec(|(loc, preiv, stat): (ReadStorage<entity::PlaneLocation>, ReadStorage<entity::Preview>, ReadStorage<entity::Status>)| {
                for (loc, preiv, stat) in (&loc, &preiv, stat.maybe()).join() {
                    draw_list
                        .add_triangle(
                            point2world(&loc, preiv.radius, 0.),
                            point2world(&loc, -preiv.radius, preiv.radius),
                            point2world(&loc, -preiv.radius, -preiv.radius),
                            preiv.color,
                        ).thickness(3.)
                        .build();
                    if let Some(entity::Status { ref text }) = stat {
                        draw_list.add_text(
                            [axis2world(loc.x), axis2world(loc.y + preiv.radius)],
                            preiv.color,
                            text,
                        )
                    }
                }
            });
            world_window.end(&ui);
        }

        if let Some(bots_window) = imgui::Window::new(im_str!("Bots"))
            .always_auto_resize(true)
            .begin(&ui)
        {
            if ui.button(im_str!("Reload ./bots"), [WINDOW_SIZE as f32 / 4., 32.]) {
                bot_modules.clear();
                load_bots = true;
            }

            if load_bots {
                load_bots = false;
                for entry in glob("bots/**/*.wasm")? {
                    bot_modules.insert(entry?.canonicalize()?, None);
                }
            }

            imgui::ColorPicker::new(im_str!("Bot Color"), &mut bot_color)
                .display_hsv(false)
                .display_rgb(false)
                .build(&ui);

            imgui::ComboBox::new(im_str!("Bot Type"))
                .build_simple_string(&ui, &mut bot_type, &[
                    im_str!("Stationary"),
                    im_str!("Treaded"),
                    im_str!("Legged"),
                ]);

            for (path, module) in bot_modules.iter_mut() {
                let label = match path.strip_prefix(&bots_root) {
                    Ok(p) => format!("+ {}", p.display()),
                    Err(_) => format!("+ {}", path.display()),
                };
                let label = imgui::ImString::new(label);
                if ui.button(&label, [WINDOW_SIZE as f32 / 4., 32.]) {
                    if module.is_none() {
                        let mut module_bytes = Vec::new();
                        if let Err(e) = File::open(path).and_then(|mut file| file.read_to_end(&mut module_bytes)) {
                            bot_error = Some(ImString::new(e.to_string()));
                            continue
                        }
                        match compile(&module_bytes) {
                            Ok(m) => *module = Some(m),
                            Err(e) => bot_error = Some(ImString::new(e.to_string())),
                        }
                    }
                    if let Some(ref module) = module {
                        match entity::EntityDriver::boot(module) {
                            Ok(r) => {
                                create_rover(&mut world, r, bot_color, bot_type);
                                bot_error = None;
                            },
                            Err(e) => bot_error = Some(ImString::new(e.to_string())),
                        }
                    }
                }
            }

            if let Some(ref e) = bot_error {
                let style = ui.push_style_color(StyleColor::Text, [1., 0.2, 0.2, 1.]);
                ui.text_wrapped(e);
                style.pop(&ui);
            }

            bots_window.end(&ui);
        }

        imgui_sdl2.prepare_render(&ui, &window);
        imgui_renderer.render(ui);
        window.gl_swap_window();
    }

    Ok(())
}
