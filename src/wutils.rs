use wasmer_runtime_core::memory::{Memory, MemoryView};
use std::io;
use std::ops::Range;
use std::convert::TryInto;

pub struct MemoryReader<'a> {
    pub view: MemoryView<'a, u8>,
    pub range: Range<usize>,
}

impl<'a> MemoryReader<'a> {
    pub fn from_i_ptr_len(mem: &'a Memory, ptr: i32, len: i32) -> Self {
        MemoryReader {
            view: mem.view(),
            range: ptr.try_into().unwrap_or(0)..(ptr + len).try_into().unwrap_or(0),
        }
    }
}

impl<'a> io::Read for MemoryReader<'a> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let src = match self.view.get(self.range.clone()) {
            Some(src) => src,
            None => return Err(io::ErrorKind::UnexpectedEof.into()),
        };
        let mut count = 0;
        for (dst, src) in buf.iter_mut().zip(src.iter()) {
            count += 1;
            *dst = src.get();
        }
        self.range.start += count;
        Ok(count)
    }
}
