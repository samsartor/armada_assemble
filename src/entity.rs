use specs::prelude::*;
use specs::{SystemData, Component};
use wasmer_runtime::{
    imports,
    Func,
    ImportObject,
    Module,
    Instance,
    Memory,
    Ctx,
    Export,
    Value, error as wasmerr,
};
use std::time;
use std::fmt;

pub struct GlobalInfo {
    pub wall_time: time::Instant,
    pub gps_time: time::Duration,
    pub delta_time: f32,
}

#[derive(Component, Default)]
#[storage(VecStorage)]
pub struct PlaneLocation {
    pub x: f32,
    pub y: f32,
    pub direction: f32,
}

impl PlaneLocation {
    pub fn transform(&self, x: f32, y: f32) -> [f32; 2] {
        let sin = self.direction.sin();
        let cos = self.direction.cos();
        [self.x + x * cos - y * sin, self.y + x * sin + y * cos]
    }
}

#[derive(Component, Default)]
#[storage(VecStorage)]
pub struct Preview {
    pub color: [f32; 4],
    pub radius: f32,
}

#[derive(Component, Default)]
pub struct Treads {
    pub radius: f32,
    pub max_acc: f32,
    pub max_vel: f32,
    pub left_vel: f32,
    pub right_vel: f32,
    pub set_left_vel: f32,
    pub set_right_vel: f32,
}

#[derive(Component, Default)]
pub struct Legs {
    pub max_reach: f32,
    pub max_turn: f32,
    pub period: f32,
    pub start: [f32; 3],
    pub vector: Option<[f32; 3]>,
    pub time: f32,
}

#[derive(Component, Default)]
#[storage(VecStorage)]
pub struct Status {
    pub text: String,
}

#[derive(Component, Default)]
pub struct Gps {
    pub memmap_ptr: Option<u32>,
}

#[derive(Default)]
pub struct EntityDriverData<'a> {
    pub global: Option<&'a GlobalInfo>,
    pub status: Option<&'a mut Status>,
    pub plane_location: Option<&'a PlaneLocation>,
    pub treads: Option<&'a mut Treads>,
    pub legs: Option<&'a mut Legs>,
    pub gps: Option<&'a mut Gps>,
}

pub struct EntityDriverContext<'a> {
    pub data: &'a mut EntityDriverData<'a>,
    pub memory: &'a Memory,
}

#[derive(Debug, Clone, Copy)]
pub struct ApiUnavailable(&'static [&'static str]);

impl fmt::Display for ApiUnavailable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "tried to use unavailable APIs: {}", self.0.join(", "))
    }
}

impl<'a> EntityDriverContext<'a> {
    pub unsafe fn from_ctx<'c: 'a>(ctx: &'c mut Ctx) -> Self {
        EntityDriverContext {
            data: &mut *(ctx.data as *mut EntityDriverData<'a>),
            memory: ctx.memory(0),
        }
    }

    pub fn update_gps(&mut self) -> bool {
        if let EntityDriverData {
            global: Some(global),
            gps: Some(&mut Gps { memmap_ptr: Some(ptr) }),
            plane_location: Some(&PlaneLocation { x, y, .. }),
            ..
        } = self.data {
            let memory = self.memory.view::<u32>();
            let index = ptr as usize >> 2;
            memory[index].set(x.to_bits());
            memory[index + 1].set(y.to_bits());
            memory[index + 2].set((global.gps_time.as_secs() & 0xffffffff) as u32);
            memory[index + 3].set(global.gps_time.subsec_micros());
            true
        } else {
            false
        }
    }

    pub fn gps_memmap(&mut self, ptr: i32) -> Result<(), ApiUnavailable> {
        match self.data {
            EntityDriverData { gps: Some(gps), .. } => {
                gps.memmap_ptr = Some(ptr as u32);
                self.update_gps();
                Ok(())
            },
            _ => Err(ApiUnavailable(&["gps"])),
        }
    }

    pub fn treads_set_speed(&mut self, left: f32, right: f32) -> Result<(), ApiUnavailable> {
        match self.data {
            EntityDriverData { treads: Some(treads), .. } => {
                treads.set_left_vel = left;
                treads.set_right_vel = right;
                Ok(())
            },
            _ => Err(ApiUnavailable(&["treads"])),
        }
    }

    pub fn legs_step_towards(&mut self, forward: f32, right: f32, turn: f32) -> Result<(), ApiUnavailable> {
        match self.data {
            EntityDriverData { plane_location: Some(loc), legs: Some(legs), .. } => {
                legs.start = [loc.x, loc.y, loc.direction];
                let mut vec = [forward / legs.max_reach, right / legs.max_reach, turn / legs.max_turn];
                let norm = vec.iter().map(|x| x * x).sum::<f32>().sqrt();
                if norm > 1. {
                    for x in vec.iter_mut() { *x /= norm; }
                }
                let [forward, right, turn] = vec;
                let [x, y] = loc.transform(forward * legs.max_reach, right * legs.max_reach);
                legs.vector = Some([x - loc.x, y - loc.y, turn * legs.max_turn]);
                legs.time = 0.;
                Ok(())
            },
            _ => Err(ApiUnavailable(&["legs"])),
        }
    }

    pub fn legs_progress(&self) -> Result<f32, ApiUnavailable> {
        match self.data {
            EntityDriverData { legs: Some(&mut Legs { vector: Some(_), time, period, ..}), .. } => Ok(time / period),
            EntityDriverData { legs: Some(_), .. } => Ok(1.),
            _ => Err(ApiUnavailable(&["legs"])),
        }
    }

    pub fn status_set_utf8(&mut self, ptr: i32, len: i32) -> Result<(), ApiUnavailable> {
        match self.data {
            EntityDriverData { status: Some(Status { text }), .. } => text.clear(),
            _ => return Err(ApiUnavailable(&["status"])),
        }
        self.status_append_utf8(ptr, len)
    }

    pub fn status_append_utf8(&mut self, ptr: i32, len: i32) -> Result<(), ApiUnavailable> {
        use crate::wutils::MemoryReader;
        use std::io::Read;

        match self.data {
            EntityDriverData { status: Some(Status { text }), .. } => {
                let _ = MemoryReader::from_i_ptr_len(self.memory, ptr, len).read_to_string(text);
                Ok(())
            },
            _ => Err(ApiUnavailable(&["status"])),
        }
    }
}

fn get_imports() -> ImportObject {
    unsafe { imports! {
        "treads" => {
            "available" => Func::new(|ctx: &mut Ctx| EntityDriverContext::from_ctx(ctx).data.treads.is_some() as i32),
            "set_speed" => Func::new(|ctx: &mut Ctx, left: f32, right: f32|
                EntityDriverContext::from_ctx(ctx).treads_set_speed(left, right)),
        },
        "legs" => {
            "available" => Func::new(|ctx: &mut Ctx| EntityDriverContext::from_ctx(ctx).data.legs.is_some() as i32),
            "progress" => Func::new(|ctx: &mut Ctx| EntityDriverContext::from_ctx(ctx).legs_progress()),
            "step_towards" => Func::new(|ctx: &mut Ctx, forward: f32, right: f32, turn: f32|
                EntityDriverContext::from_ctx(ctx).legs_step_towards(forward, right, turn)),
        },
        "gps" => {
            "available" => Func::new(|ctx: &mut Ctx| EntityDriverContext::from_ctx(ctx).data.gps.is_some() as i32),
            "memmap" => Func::new(|ctx: &mut Ctx, ptr: i32|
                EntityDriverContext::from_ctx(ctx).gps_memmap(ptr)),
        },
        "status" => {
            "available" => Func::new(|ctx: &mut Ctx| EntityDriverContext::from_ctx(ctx).data.status.is_some() as i32),
            "set_utf8" => Func::new(|ctx: &mut Ctx, ptr: i32, len: i32|
                EntityDriverContext::from_ctx(ctx).status_set_utf8(ptr, len)),
            "append_utf8" => Func::new(|ctx: &mut Ctx, ptr: i32, len: i32|
                EntityDriverContext::from_ctx(ctx).status_append_utf8(ptr, len)),
        },
        "sys" => {
            "shutdown" => Func::new(|_: &mut Ctx| -> Result<(), &'static str> { Err("shutdown called") }),
        },
    } }
}

impl<'a> EntityDriverData<'a> {
    
}

#[derive(Component)]
pub struct EntityDriver {
    instance: Instance,
    called_init: bool,
    pub crash: Option<String>,
}

impl EntityDriver {
    pub fn boot(module: &Module) -> wasmerr::Result<Self> {
        let mut import = imports! {
            || (
                Box::into_raw(Box::new(EntityDriverData::default())) as _,
                |ptr| unsafe { Box::from_raw(ptr as *mut EntityDriverData<'static>); }
            ),
        };
        import.extend(get_imports());
        Ok(EntityDriver {
            instance: module.instantiate(&import)?,
            called_init: false,
            crash: None,
        })
    }

    pub fn reboot(&mut self) -> wasmerr::Result<()> {
        *self = EntityDriver::boot(&self.instance.module())?;
        Ok(())
    }

    pub fn with_data<'r, 's: 'r>(&'s mut self, data: EntityDriverData<'r>) -> EntityDriverRef<'r> {
        unsafe {
            // Context data can only be accessed (directly or indirectly)
            // through the returned ref which is only allowed to live as long as
            // 'r. Thus, we can put any borrows that live at least as long as 'r
            // into the context data.
            *(self.instance.context_mut().data as *mut EntityDriverData<'r>)= data;
        }
        EntityDriverRef { driver: self }
    }
}

// The only way to access the Instance is with an exclusive loan (&mut). Thus,
// shared loans can be sent freely between threads. We must simply make sure that
// NO ACCESSES ARE EVERY MADE TO INSTANCE WITHOUT A &mut.
unsafe impl Sync for EntityDriver { }

pub struct EntityDriverRef<'a> {
    driver: &'a mut EntityDriver,
}

impl<'a> EntityDriverRef<'a> {
    pub fn context(&mut self) -> EntityDriverContext {
        unsafe { EntityDriverContext::from_ctx(self.driver.instance.context_mut()) }
    }

    pub fn try_init(&mut self) {
        if !self.driver.called_init {
            if self.driver.instance.exports().find(|ex| match (ex.0.as_ref(), &ex.1) {
                ("init", Export::Function { .. }) => true,
                _ => false,
            }).is_some() {
                let _ = self.call("init", &[]);
            }
            self.driver.called_init = true;
        }
    }

    pub fn call(&mut self, name: &str, params: &[Value]) -> wasmerr::CallResult<Vec<Value>> {
        let res = self.driver.instance.call(name, params);
        if let Err(ref e) = res {
            let msg = match e {
                wasmerr::CallError::Runtime(wasmerr::RuntimeError::Error { data }) => match data.downcast_ref::<ApiUnavailable>() {
                    Some(e) => e.to_string(),
                    _ => e.to_string(),
                },
                _ => e.to_string(),
            };
            eprintln!("driver crash calling \"{}\" with {:?}: {}", name, params, msg);
            if let Some(Status { text }) = self.context().data.status {
                *text = format!("robot crashed: {}", msg);
            }
            self.driver.crash = Some(msg);
        }
        res
    }
}

#[derive(SystemData)]
pub struct EntityDriverSystemData<'a> {
    pub global: ReadExpect<'a, GlobalInfo>,
    pub drivers: WriteStorage<'a, EntityDriver>,
    pub status: WriteStorage<'a, Status>,
    pub plane_location: ReadStorage<'a, PlaneLocation>,
    pub treads: WriteStorage<'a, Treads>,
    pub legs: WriteStorage<'a, Legs>,
    pub gps: WriteStorage<'a, Gps>,
}

pub struct EntityDriverSystem;

impl<'a> System<'a> for EntityDriverSystem {
    type SystemData = EntityDriverSystemData<'a>;

    fn run(&mut self, mut sys_data: EntityDriverSystemData) {
        for data in (
            &mut sys_data.drivers,
            (&mut sys_data.status).maybe(),
            sys_data.plane_location.maybe(),
            (&mut sys_data.treads).maybe(),
            (&mut sys_data.legs).maybe(),
            (&mut sys_data.gps).maybe(),
        ).join() {
            let driver: &mut EntityDriver = data.0;
            if driver.crash.is_some() {
                continue;
            }

            let mut ready_driver = driver.with_data(EntityDriverData {
                global: Some(&*sys_data.global),
                status: data.1,
                plane_location: data.2,
                treads: data.3,
                legs: data.4,
                gps: data.5,
            });

            let mut ctx = ready_driver.context();
            ctx.update_gps();

            ready_driver.try_init();
            let _ = ready_driver.call("step", &[]);
        }
    }
}

pub struct TreadSimulationSystem;

impl<'a> System<'a> for TreadSimulationSystem {
    type SystemData = (ReadExpect<'a, GlobalInfo>, WriteStorage<'a, Treads>, WriteStorage<'a, PlaneLocation>);

    fn run(&mut self, (ref info, ref mut tread_comp, ref mut loc_comp): Self::SystemData) {
        for (tread, loc) in (tread_comp, loc_comp).join() {
            let max_dvel = tread.max_acc * info.delta_time;
            tread.left_vel = (
                (tread.set_left_vel - tread.left_vel).min(max_dvel).max(-max_dvel)
                + tread.left_vel)
                .min(tread.max_vel).max(-tread.max_vel);
            tread.right_vel = (
                (tread.set_right_vel - tread.right_vel).min(max_dvel).max(-max_dvel)
                + tread.right_vel)
                .min(tread.max_vel).max(-tread.max_vel);
            loc.direction += (tread.right_vel - tread.left_vel) * info.delta_time / tread.radius;
            let vel = (tread.left_vel + tread.right_vel) / 2.;
            loc.x += vel * loc.direction.cos() * info.delta_time;
            loc.y += vel * loc.direction.sin() * info.delta_time;
        }
    }
}

pub struct LegsSimulationSystem;

impl<'a> System<'a> for LegsSimulationSystem {
    type SystemData = (ReadExpect<'a, GlobalInfo>, WriteStorage<'a, Legs>, WriteStorage<'a, PlaneLocation>);

    fn run(&mut self, (ref info, ref mut legs_comp, ref mut loc_comp): Self::SystemData) {
        for (legs, loc) in (legs_comp, loc_comp).join() {
            if let Some([dx, dy, dr]) = legs.vector {
                let [x, y, r] = legs.start;
                let t = legs.time / legs.period;
                if t > 1. {
                    loc.x = x + dx;
                    loc.y = y + dy;
                    loc.direction = r + dr;
                    legs.vector = None;
                } else {
                    loc.x = x + dx * t;
                    loc.y = y + dy * t;
                    loc.direction = r + dr * t;
                    legs.time += info.delta_time;
                }
            }
        }
    }
}

pub struct PreStepSystem {
    pub max_step: time::Duration,
}

impl<'a> System<'a> for PreStepSystem {
    type SystemData = WriteExpect<'a, GlobalInfo>;

    fn run(&mut self, mut info: Self::SystemData) {
        let mut info: &mut GlobalInfo = &mut *info;
        let now = time::Instant::now();
        let diff = now.duration_since(info.wall_time).min(self.max_step);
        info.wall_time = now;
        info.delta_time = diff.as_secs_f32();
        info.gps_time += diff;
    }
}
